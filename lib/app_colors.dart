import 'package:flutter/material.dart';

class AppColor {
  static const appBarGradientBegin = Color(0xFF3a82f5);
  static const appBarGradientEnd = Color(0xFF00c6ff);
  static const canvasColor = Color(0xFF3e3963);
  static const cardColor = Color(0xFF434273);

  static const primaryTextColor = Colors.white;
  static const secondaryTextColor = Color(0xffb0acda);


  static const iconColor = Color(0xff746fa3);

}